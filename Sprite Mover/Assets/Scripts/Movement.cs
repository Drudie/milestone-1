﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Transform tf; //A variable to hold the transform component.
    public int speed;  //A variable for anyone to edit the move speed within Unity.

    // Start is called before the first frame update
    void Start()
    {
        //Get the transform component
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //Moves the object to the right based on the speed provided
        tf.position = tf.position + (Vector3.right * speed);
    }
}
